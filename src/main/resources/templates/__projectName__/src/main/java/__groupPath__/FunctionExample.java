package @group@;

import java.util.function.Function;
import org.springframework.stereotype.Component;

@Component("FunctionExample")
public class FunctionExample implements Function<String, String> {

  @Override
  public String apply(String input) {
    return input.toUpperCase();
  }

}